import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GreeterTest {
    private final Greeter greeter = new Greeter();

    @Test
    void same_day_of_month() {
        String dateToday = "2022/10/24";
        String dateToCompare = "2018/10/24";

        assertTrue(greeter.hasSameDayOfMonth(dateToday, dateToCompare));
    }

    @Test
    void not_same_day_of_month() {
        String dateToday = "2022/10/24";
        String dateToCompare = "2018/10/10";

        assertFalse(greeter.hasSameDayOfMonth(dateToday, dateToCompare));
    }

    @Test
    void has_birthday_today() {
        assertTrue(greeter.hasBirthday("2022/10/24", "2018/10/24"));
    }

    @Test
    void does_not_have_birthday_today() {
        assertFalse(greeter.hasBirthday("2022/10/24", "2018/10/10"));
    }

    @Test
    void has_birthday_today_for_leap_year_birthday() {
        assertTrue(greeter.hasBirthday("1999/03/01", "1996/02/29"));
    }

    @Test
    void datei_liefert_personen_daten() throws Exception {

        Path filePath = Files.createTempFile("greeterPersonen", ".csv");
        Files.write(filePath, List.of("last_name,first_name,date_of_birth,email",
                "Doe,John,1982/10/08,john.doe@foobar.com",
                "Ann,Mary,1975/09/11,mary.ann@foobar.com"));

        List<Person> personen = List.of(new Person("John", "Doe", "1982/10/08", "john.doe@foobar.com"), new Person("Mary", "Ann", "1975/09/11", "mary.ann@foobar.com"));
        Assertions.assertEquals(personen, greeter.readGreeterPersonenFromFile(filePath.toAbsolutePath().toString()));
    }

     @Test
     void keine_nachrichten_bei_leerer_datenquelle(){
        TestVersand testVersand = new TestVersand();
        TestDatenquelle testDatenquelle = new TestDatenquelle(List.of());

        greeter.sendeNachrichten("2022/12/24", testVersand, testDatenquelle);

        Assertions.assertEquals(List.of(), testVersand.sentMessages());
     }

    @Test
    void keine_nachrichten_wenn_keine_person_mit_geburtstag_vorliegt() {
        TestVersand testVersand = new TestVersand();
        TestDatenquelle testDatenquelle = new TestDatenquelle(List.of(new Person("Max", "Mustermann", "2000/12/25", "max.mustermann@qwer.de")));

        greeter.sendeNachrichten("2022/12/24", testVersand, testDatenquelle);

        Assertions.assertEquals(List.of(), testVersand.sentMessages());
    }

    @Test
    void eine_nachricht_wenn_eine_person_mit_geburtstag_vorliegt() {
        TestVersand testVersand = new TestVersand();
        TestDatenquelle testDatenquelle = new TestDatenquelle(List.of(new Person("Max", "Mustermann", "2000/12/24", "max.mustermann@qwer.de")));

        greeter.sendeNachrichten("2022/12/24", testVersand, testDatenquelle);

        Assertions.assertEquals(List.of("Happy birthday, dear Max!"), testVersand.sentMessages());
    }

    @Test
    void zwei_nachrichten_wenn_zwei_personen_mit_geburtstag_vorliegen() {
        TestVersand testVersand = new TestVersand();
        List<Person> personData = List.of(new Person("Max", "Mustermann", "2000/12/24", "max.mustermann@qwer.de"),
                new Person("Maxima", "Mustermann", "1991/12/24", "maxima.mustermann@qwer.de"));
        TestDatenquelle testDatenquelle = new TestDatenquelle(personData);

        greeter.sendeNachrichten("2022/12/24", testVersand, testDatenquelle);

        Assertions.assertEquals(List.of("Happy birthday, dear Max!", "Happy birthday, dear Maxima!"), testVersand.sentMessages());
    }

    @ParameterizedTest
    @ValueSource(ints = {
            1996,
            2000,
    })
    void is_leap_year(int year) {
        assertTrue(greeter.isLeapYear(year));
    }

    @ParameterizedTest
    @ValueSource(ints = {
            1900,
            1997,
    })
    void not_leap_year(int year) {
        assertFalse(greeter.isLeapYear(year));
    }
}