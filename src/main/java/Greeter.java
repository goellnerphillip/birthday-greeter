import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Greeter {

    /*
     * Mail versenden
     * Datei auslesen -> timestamps -> DTO
     * Geburtstage ermitteln -> vergleichen auf Tag + Monat
     */

    public void sendeNachrichten(String datum, VersandStrategy versender, Datenquelle datenquelle) {
        // Generierung der Nachrichten...

        final List<String> nachrichten = datenquelle
                .getPersonData().stream()
                .filter(person -> this.hasBirthday(datum, person.dateOfBirth()))
                .map(this::createPersonGreeting)
                .toList();

        versender.sendMessages(nachrichten);
    }

    private String createPersonGreeting(Person person) {
        return String.format("Happy birthday, dear %s!", person.firstName());
    }

    public boolean hasSameDayOfMonth(String dateToday, String dateToCompare) {
        String dayOfMonthToday = dateToday.substring(5);
        String dayOfMonthToCompare = dateToCompare.substring(5);

        return dayOfMonthToday.equals(dayOfMonthToCompare);
    }

    public boolean hasBirthday(String dateToday, String dateOfBirth) {
        boolean todayIsBirthday;

        if (dateOfBirth.endsWith("02/29")) {
            boolean thisYearIsLeapYear = isLeapYear(Integer.parseInt(dateToday.substring(0, 4)));

            todayIsBirthday = !thisYearIsLeapYear && dateToday.endsWith("03/01");
        } else {
            todayIsBirthday = hasSameDayOfMonth(dateToday, dateOfBirth);
        }

        return todayIsBirthday;
    }

    public boolean isLeapYear(int year) {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }

    public List<Person> readGreeterPersonenFromFile(String fileName) throws IOException, URISyntaxException {

        return Files.lines(Paths.get(fileName)).skip(1).map(line -> {
            String[] personCsv = line.split(",");
            String lastName = personCsv[0];
            String firstName = personCsv[1];
            String dateOfBirth = personCsv[2];
            String email = personCsv[3];

            return new Person(firstName, lastName, dateOfBirth, email);
        }).toList();
    }


}
