import java.util.List;

public class TestVersand implements VersandStrategy {

    private List<String> sentMessages;

    List<String> sentMessages() {
        return sentMessages;
    }

    @Override
    public void sendMessages(List<String> messages) {
        sentMessages = messages.stream().toList();
    }
}
