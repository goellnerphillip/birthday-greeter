import java.util.List;

public class TestDatenquelle implements Datenquelle {

    private final List<Person> personData;

    public TestDatenquelle(List<Person> personData) {
        this.personData = personData.stream().toList();
    }

    public List<Person> getPersonData() {
        return this.personData;
    }

}
