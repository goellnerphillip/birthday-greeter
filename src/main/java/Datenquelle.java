import java.util.List;

public interface Datenquelle {

    List<Person> getPersonData();

}
